package com.yaraza.causita.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class PlatilloDTO implements Serializable {

	private long idPlatillo;
	private String nomPlatillo;
	private String desPlatillo;
	private String precioPlatillo;
	private String imagenPlatillo;

	public PlatilloDTO(long idPlatillo, String nomPlatillo, String desPlatillo, String precioPlatillo,
			String imagenPlatillo) {
		super();
		this.idPlatillo = idPlatillo;
		this.nomPlatillo = nomPlatillo;
		this.desPlatillo = desPlatillo;
		this.precioPlatillo = precioPlatillo;
		this.imagenPlatillo = imagenPlatillo;
	}

	public PlatilloDTO() {
	}

	public long getIdPlatillo() {
		return idPlatillo;
	}

	public void setIdPlatillo(long idPlatillo) {
		this.idPlatillo = idPlatillo;
	}

	public String getNomPlatillo() {
		return nomPlatillo;
	}

	public void setNomPlatillo(String nomPlatillo) {
		this.nomPlatillo = nomPlatillo;
	}

	public String getDesPlatillo() {
		return desPlatillo;
	}

	public void setDesPlatillo(String desPlatillo) {
		this.desPlatillo = desPlatillo;
	}

	public String getPrecioPlatillo() {
		return precioPlatillo;
	}

	public void setPrecioPlatillo(String precioPlatillo) {
		this.precioPlatillo = precioPlatillo;
	}

	public String getImagenPlatillo() {
		return imagenPlatillo;
	}

	public void setImagenPlatillo(String imagenPlatillo) {
		this.imagenPlatillo = imagenPlatillo;
	}

}
