package com.yaraza.causita.dto;

import java.util.List;

public class ResultadoPlatillosDTO {

	private int cantidadItems;
	private String fechaBusqueda;
	private List<PlatilloDTO> listaPlatillos;

	public ResultadoPlatillosDTO(int cantidadItems, String fechaBusqueda, List<PlatilloDTO> listaPlatillos) {
		super();
		this.cantidadItems = cantidadItems;
		this.fechaBusqueda = fechaBusqueda;
		this.listaPlatillos = listaPlatillos;
	}

	public ResultadoPlatillosDTO() {
	}

	public int getCantidadItems() {
		return cantidadItems;
	}

	public void setCantidadItems(int cantidadItems) {
		this.cantidadItems = cantidadItems;
	}

	public String getFechaBusqueda() {
		return fechaBusqueda;
	}

	public void setFechaBusqueda(String fechaBusqueda) {
		this.fechaBusqueda = fechaBusqueda;
	}

	public List<PlatilloDTO> getListaPlatillos() {
		return listaPlatillos;
	}

	public void setListaPlatillos(List<PlatilloDTO> listaPlatillos) {
		this.listaPlatillos = listaPlatillos;
	}

}
