package com.yaraza.causita.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.yaraza.causita.dao.YarazaCausitaDAO;
import com.yaraza.causita.dao.impl.YarazaCausitaDAOImpl;
import com.yaraza.causita.dto.ResultadoPlatillosDTO;

@Path("/v1/rest/get")
public class YarazaCausitaRest {

	@GET
	@Path("/listadoPlatillos")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultadoPlatillosDTO listadoPlatillos() {
		ResultadoPlatillosDTO resultadoPlatillos = new ResultadoPlatillosDTO();
		YarazaCausitaDAO dao = new YarazaCausitaDAOImpl();
		resultadoPlatillos = dao.obtenerListaPlatillos();
		return resultadoPlatillos;
	}

}
