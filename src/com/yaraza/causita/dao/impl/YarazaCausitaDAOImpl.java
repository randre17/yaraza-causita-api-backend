package com.yaraza.causita.dao.impl;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.yaraza.causita.bd.util.UtilBD;
import com.yaraza.causita.dao.YarazaCausitaDAO;
import com.yaraza.causita.dto.PlatilloDTO;
import com.yaraza.causita.dto.ResultadoPlatillosDTO;

public class YarazaCausitaDAOImpl implements YarazaCausitaDAO {

	@Override
	public ResultadoPlatillosDTO obtenerListaPlatillos() {
		ResultadoPlatillosDTO resultado = new ResultadoPlatillosDTO();
		List<PlatilloDTO> listaPlatillos = new ArrayList<>();
		int contador = 0;
		Date fechaActual = new Date();
		try {
			Statement statement = UtilBD.getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM tb_platillo");
			PlatilloDTO item = null;
			while (resultSet.next()) {
				item = new PlatilloDTO();
				item.setIdPlatillo(resultSet.getLong("id"));
				item.setNomPlatillo(resultSet.getString("nombre"));
				item.setDesPlatillo(resultSet.getString("descripcion"));
				item.setPrecioPlatillo(resultSet.getString("precio"));
				item.setImagenPlatillo(resultSet.getString("imagen"));
				listaPlatillos.add(item);
				contador++;
			}
			resultSet.close();
			statement.close();

			resultado.setCantidadItems(contador);
			resultado.setFechaBusqueda(fechaActual.toString());
			resultado.setListaPlatillos(listaPlatillos);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultado;
	}

}
