DROP DATABASE IF EXISTS bd_yaraza_causita;
CREATE DATABASE IF NOT EXISTS bd_yaraza_causita;
USE bd_yaraza_causita;

CREATE TABLE tb_platillo(
	id int auto_increment,
	nombre varchar(200) not null,
    descripcion varchar(500) not null,
    precio varchar(20) not null,
    imagen varchar (500) not null,
    primary key (id)
);

insert into tb_platillo(nombre,descripcion, precio, imagen) values 
('Tallarines Verdes','Tallarines verdes con milanesa','25.00','https://res.cloudinary.com/glovoapp/w_200,h_200,c_fit,f_auto,q_auto/Products/v6tmxtb7q1xedhknffjj'),
('Tallarines a la huancaina','Tallarín a la huancaína con elección de acompañamiento.','31.90','https://res.cloudinary.com/glovoapp/w_680,h_240,c_fit,f_auto,q_auto/Products/dowyzqxzzhrd1w0hdno5'),
('Lomo Saltado','Lomo saltado, papa a la huancaína','32.00','https://res.cloudinary.com/glovoapp/w_680,h_240,c_fit,f_auto,q_auto/Products/vlttvc3yvhacxmt1dxzh'),
('Milanesa Napolitana','Milanesa Napolitana con jamón y tallarines en salsa blanca + 2 panes al ajo + bebida','28.00','https://res.cloudinary.com/glovoapp/w_680,h_240,c_fit,f_auto,q_auto/Products/x2n24n7sutla0hnop8si'),
('Alfredo','Alfredo con milanesa de pollo.','18.00','https://res.cloudinary.com/glovoapp/w_680,h_240,c_fit,f_auto,q_auto/Products/sap1te1fzxorfsgpbb6t'),
('Milanesa Triple Queso','Milanesa triple queso','22.00','https://res.cloudinary.com/glovoapp/w_680,h_240,c_fit,f_auto,q_auto/Products/fdtwpzjekg5ski4qhnmx'),
('Miso Ramen (1 porcion)','Sopa de fideo ramen japonés y caldo a base de miso (soya), acompañado de chancho','32.00','https://res.cloudinary.com/glovoapp/w_680,h_240,c_fit,f_auto,q_auto/Products/yaizkndqllmlwefgxr6y'),
('Chicken Katsu','Pollo empanizado al panko, acompañado de ensalada de col y arroz japonés','29.00','https://res.cloudinary.com/glovoapp/w_680,h_240,c_fit,f_auto,q_auto/Products/k4wfrzytrwuahfesw26z'),
('Crispy Roll (5 cortes)','Relleno de langostino empanizado, queso crema y palta, cubierto con tenkatsu ','20.00','https://res.cloudinary.com/glovoapp/w_680,h_240,c_fit,f_auto,q_auto/Products/odb3gbazoak90vwxzv4l'),
('Costillas originales en Salsa BBQ (1/2 rack)','Costillas originales en Salsa BBQ (1/2 rack).','36.00','https://res.cloudinary.com/glovoapp/w_680,h_240,c_fit,f_auto,q_auto/Products/lpjhmhy4rorneopd4hwr'),
('Chop Chop New York','Pollo al grill en salsa jack, tomate, huevo de corral y arrocito.','26.00','https://res.cloudinary.com/glovoapp/w_680,h_240,c_fit,f_auto,q_auto/Products/i5rtl9o7shrobataeovu');